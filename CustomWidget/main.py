import kivy
kivy.require("1.9.0")
from kivy.app import App
from kivy.uix.gridlayout import GridLayout

class CalGridLayout(GridLayout):
    pass

    def calculate(self, calculation):
        if calculation:
            try:
                self.display.text = str(eval(calculation))
            except Exception:
                self.display.text = "Error"


class CalculatorApp(App):


    def build(self):
        return CalGridLayout()

calculator = CalculatorApp()
calculator.run()